
var apiHost = "";
var relationsUrl = "";
var addNewUrl = "";

var lastItemSelected = new Array();
lastItemSelected['type'] = '';
lastItemSelected['id'] = 0;

var lastLineSelected = 0;
var lastFacilitySelected = 0;
var lastProgramSelected = 0;
var lastCenterSelected = 0;

function init(_lines, _domains, _centers, _facilities)
{
    var lines = jQuery.map(_lines, function(el) { return el; });
    jQuery('input:checkbox').attr("disabled", true);
    
}

function setApiHost(value)
{
    apiHost = value;
}

function updateAddNewUrl(urlString){
    addNewUrl = urlString;
}

function updateRelationsUrl(urlString){
    relationsUrl = urlString;
}


function onItemChange(type, id, params){
    
    jQuery('.edit-relations-btn').show();
    jQuery('#edit-relations-'+type+'-'+id).hide();

console.log("onItemChange");
console.log(type);
console.log(id);
console.log(params);

//toggleEditBtns(type, id);
toggleChecksBoxes(type, id, params);
   
   lastItemSelected['type'] = type;
   lastItemSelected['id'] = id;
   
    if(type === 'line')
    {
        lastLineSelected = id;
    }
    else if (type === 'domain')
    {
       lastProgramSelected = id;
    }
    else if (type === 'facility')
    {
       lastFacilitySelected = id;
    }
    else
    {
       lastCenterSelected = id;
    }
}

function toggleEditBtns(type,k){
    jQuery('.edit-btn').hide();
    jQuery('#'+type+'-edit-btn-'+k).show();
}

function onItemEdit(type, id){
        /*console.log("onItemEdit");
        console.log(type);
        console.log(id);*/
    
        jQuery('#index-alert').remove();
        var modal = jQuery('#addModal');
        initForm('Edit', type);
        
        var url = jQuery('#get-'+type+'-url').val();
        
        getItem(url, id, type,populateEditForm);
        
        modal.modal('show');
}

function populateEditForm(type, info){
   
    /*console.log('***info***');
    console.log(info);*/
    
    jQuery('#type').val(type);
    jQuery('#image').hide();
    
    if(info.img_source != null){
        jQuery('#image').attr("src", info.img_source).show();
    }
   
    jQuery('#state').val(info.state);   
    jQuery('#state').trigger("liszt:updated");
    
    console.log(info.state);
    
    if(type === 'center')
    {
        jQuery('#id').val(info.id);      
        jQuery('#name').val(info.name);
        jQuery('#alias').val(info.alias);
        jQuery('#shortcut').val(info.shortcut);
        jQuery('#address').val(info.address);
        jQuery('#city').val(info.city);
        jQuery('#country').val(info.country);
        jQuery('#zip').val(info.zip);
        jQuery('#lat').val(info.lat);
        jQuery('#lng').val(info.lng);
        
    }
    else{
        jQuery('#id').val(info.id);      
        jQuery("input[name*='name-en']").val(info.name_en);
        jQuery("input[name*='name-it']").val(info.name_it);
        jQuery("input[name*='alias-en']").val(info.alias_en);
        jQuery("input[name*='alias-it']").val(info.alias_it);
        
        
        jQuery(tinyMCE.get('description_en').getBody()).html(decodeURI(info.description_en));
        jQuery(tinyMCE.get('description_it').getBody()).html(decodeURI(info.description_it));
        
    }
    
    //console.log(info.description_en);
}

function toggleChecksBoxes(selectType, id, params)
{
    highlightToggle(selectType, id);
    
    jQuery('input:checkbox').removeAttr('checked');
    jQuery('input:checkbox').removeAttr("disabled");
    jQuery('.check'+selectType).attr("disabled", true);
 
    if (selectType === 'line') {
        if(params.domains != null){
            var domains = jQuery.map(params.domains, function(el) { return el; });
            selectAffected('domain', domains);
            }
        if(params.centers != null){
            var centers = jQuery.map(params.centers, function(el) { return el; });
            selectAffected('center', centers);
            }
        if(params.facilities != null){
            var facilities = jQuery.map(params.facilities, function(el) { return el; });
            selectAffected('facility', facilities);
            }
	}
    else if (selectType === 'center') {
        if(params.lines != null){
            var lines = jQuery.map(params.lines, function(el) { return el; });
            selectAffected('line', lines);
            }
        if(params.domains != null){
            var domains = jQuery.map(params.domains, function(el) { return el; });
            selectAffected('domain', domains);
            }
        if(params.facilities != null){
            var facilities = jQuery.map(params.facilities, function(el) { return el; });
            selectAffected('facility', facilities);
            }
	}
    else if (selectType === 'facility') {
        if(params.lines != null){
            var lines = jQuery.map(params.lines, function(el) { return el; });
            selectAffected('line', lines);
            }
        if(params.domains != null){
            var domains = jQuery.map(params.domains, function(el) { return el; });
            selectAffected('domain', domains);
            }
        if(params.centers != null){
            var centers = jQuery.map(params.centers, function(el) { return el; });
            selectAffected('center', centers);
            }
	}
    else {
        if(params.lines != null){
            var lines = jQuery.map(params.lines, function(el) { return el; });
            selectAffected('line', lines);
            }
        if(params.centers != null){
            var centers = jQuery.map(params.centers, function(el) { return el; });
            selectAffected('center', centers);
            }
        if(params.facilities != null){
            var facilities = jQuery.map(params.facilities, function(el) { return el; });
            selectAffected('facility', facilities);
            }
	}
}

function highlightToggle(selectType, id)
{
    jQuery('#'+selectType+'-'+id).addClass( "selected" );
    
    if (lastLineSelected != 0) {
	jQuery('#line-'+lastLineSelected).removeClass( "selected" );
    }
    if (lastFacilitySelected != 0) {
	jQuery('#facility-'+lastFacilitySelected).removeClass( "selected" );
    }
    if (lastCenterSelected != 0) {
	jQuery('#center-'+lastCenterSelected).removeClass( "selected" );
    }
    if(lastProgramSelected != 0) {
        jQuery('#domain-'+lastProgramSelected).removeClass( "selected" );
    }
}

function selectAffected(type, arr)
{
    for(var c in arr){
      //console.log('selectAffected: #check'+type+'-'+arr[c].id);
        //console.log('selectAffected: '+type+'-'+arr[c].id);
      jQuery('#check'+type+'-'+arr[c].id).attr('checked','checked');
    }
}

function hideCheckBoxes(type)
{
    if (selectType == 'line')
	{
		jQuery('#lines').parent().show();
	}
	else if (selectType == 'center')
	{
	
	}
	else //domain
	{
	
	}
	
}

function onCheckboxAction(type, id, value)
{
    
    //var tmp = new Array();
    domainsChanges = new Array();
    centersChanges = new Array();
    console.log("type::: "+type);
    var currentId = lastItemSelected['id'];
    
    var propName = type === 'facility' ? 'facilities' : type+'s';
    //var propName = type+'s';
    
    
    linesChanges[propName][currentId] = new Array();
    jQuery('input:checkbox.check'+type).each(function () {
                if(jQuery(this).is(":checked") == true)
                {
                    //tmp.push(jQuery(this).val());
                    
                    if(type === 'domain' )
                    {
                        linesChanges['domains'][currentId].push(jQuery(this).val());
                    }
                    else if(type === 'facilities' )
                    {
                        linesChanges['domains'][currentId].push(jQuery(this).val());
                    }
                    else if(type === 'center' )
                    {
                        linesChanges['centers'][currentId].push(jQuery(this).val());
                    }
                }
           });
           
}

function updateItem(data){
    
    var txt = data.name_en;
    
    if(data.type == 'center'){
        txt = data.name;
    }
    
    jQuery('#'+data.type+'-'+data.id).html(txt);
}
    
function updateRelationsVisual(type, list){
    
    var plural = type+'s';
    
    for(var id in list){
      
      var idsStr = list[id];
      var ids = idsStr.toString().split(",");
       
        
        lines[id][plural] = new Array();
        
        for (i = 0; i < ids.length; i++) { 
            lines[id][plural].push({id:ids[i], name:""});
        }
        selectAffected(type, lines[id][plural]);
     
    }
    
}
    
     function saveRelations(saveRelationsUrl) 
     {
        var relationsData = {};
        relationsData['domains_lines'] = JSON.stringify(jQuery.extend({}, linesChanges['domains']));
        relationsData['centers_lines'] = JSON.stringify(jQuery.extend({}, linesChanges['centers']));
        relationsData['facilities_lines'] = JSON.stringify(jQuery.extend({}, linesChanges['facilities']));
        //console.log("saveRelations::: ");
        //console.log(relationsData);
        return jQuery.ajax({
            url: saveRelationsUrl,
            data: relationsData,
            type:'post'
        })
        .done(function(response) {
            /*console.log(':::data:::');
            console.log(linesChanges['domains']);
            console.log(':::response:::');
            console.log(response.data);*/
            
            if(response.done === true) 
            {
                updateRelationsVisual('domain', linesChanges['domains']);
                updateRelationsVisual('center', linesChanges['centers']);
                updateRelationsVisual('facility', linesChanges['facilities']);
                setAlert(eGlobalMessage, 'success', 'Relations saved.', 5000);
            }
            else  {
                setAlert(eGlobalMessage, 'danger', 'Error saving data. Reason: ' + response.data);
                return false;
            }
        })
        .fail(function(e) {
            setAlert(eGlobalMessage, 'danger', 'Error saving data ' + e);
            return false;
        });
        
    }
    
    
    function getItem(url, id, type, callBackFn) {
        console.log('---getItem---');
        console.log('***url***');
        console.log(url);
        
        var data = {};
        data['id'] = id;
        
        return jQuery.ajax({
            url: url,
            data: data,
            type:'post'
        })
        .done(function(response) {
            
            if(response.done === true) 
            {
                console.log('***data***');
                console.log(response.data);
                
                callBackFn(type, response.data);
                
                //return response.data;
                ///setAlert(eGlobalMessage, 'success', 'Relations saved.', 5000);
            }
            else  {
                //setAlert(eGlobalMessage, 'danger', 'Error saving data. Reason: ' + response.data);
                return false;
            }
        })
        .fail(function(e) {
            setAlert(eGlobalMessage, 'danger', 'Error saving data ' + e);
            return false;
        });
        
    }
    
  
    
    
   