<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
/**
 * relation Controller
 *
 * @package     com_relations
 */
class RelationsControllerRelation extends JControllerForm
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function delete()
    {
        $input = JFactory::getApplication()->input;
        $ids = $input->getVar('cid');
        
        $oModel = $this->getModel('relation');
        $response = $oModel->delete($ids);
        
        //die(var_dump($response));
        
        $msg = '';
        $msgType = 'message';
        if($response['status'] == 'ok') {
            $msg = $response['data'];
        }
        else {
            $msg = $response['error'];
            $msgType = 'error';
        }
        
        $this->setRedirect("index.php?option=com_relations&view=relations", $msg, $msgType);
    }
    
    protected function allowEdit($data = array(), $key = 'id')
	{
            die("allowEdit");
        
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;

		if ($recordId)
		{
                    $categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId)
		{
			// The category has been set. Check the category permissions.
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		}
		else
		{
			// Since there is no asset tracking, revert to the component permissions.
			return parent::allowEdit($data, $key);
		}
	}
    
    protected function loadFormData()
    {
        
        die("loadFormData");
        
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState(
            'com_relations.edit.relation.data',
            array()
        );
        
        if (empty($data))
        {
            $data = $this->getItem();
        }
        
        return $data;
    }
    
    
}

