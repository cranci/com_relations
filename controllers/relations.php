<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
/**
 * Relation Controller
 *
 * @package     com_relations
 */
class RelationsControllerRelations extends JControllerLegacy
{
    
    public function display()
    {
        
        //$oModel = $this->getModel();
       
        /*
	$response = $oModel->getRelations();
        $oView = $this->getView('relations', '');
        $oView->display($response);*/
    }
    
    
    public function save()
    {
        $jinput = JFactory::getApplication()->input;
        $aData = array();
     
        
        $type = $jinput->get('type');
        
        if($type == 'center')
        {
            $aData = $jinput->getArray(array(
            'id' => 'string',
            'name' => 'string',
            'shortcut' => 'string',
            'alias' => 'string',
            'country' => 'string',
            'city' => 'string',
            'address' => 'string',
            'zip' => 'string',
            'phone' => 'string',
            'lat' => 'string',
            'lng' => 'string',
            'type' => 'string',
            'state'=> 'string'
            ));
        }
        else
        {
            $aData = $jinput->getArray(array(
            'id' => 'string',
            'name_en' => 'string',
            'name_it' => 'string',
            'alias_en' => 'string',
            'alias_it' => 'string',
            'description_en' => 'string',
            'description_it' => 'string',
            'img_source' => 'string',
            'type' => 'string',
            'state'=> 'string'
            ));
        }
       
        $aData['description_en'] = urldecode($aData['description_en']);
        $aData['description_it'] = urldecode($aData['description_it']);
        
        $oView = $this->getView($type, 'json');
        $oModel = $this->getModel($type);
     
        $response = $oModel->save($aData);
        
       
        //$oView->displayJSON(array('done' => true, 'data'=>$aData));
        $oView->displayJSON($response);
    }
    
    public function updateLinesRelations(){
        $jinput = JFactory::getApplication()->input;
        $aData = array();
        $aData = $jinput->getArray(array(
            //'programs_lines' => 'string',
            'domains_lines' => 'string',
            'centers_lines' => 'string',
            'facilities_lines' => 'string'
            ));
        $oView = $this->getView('line', 'json');
        $oModel = $this->getModel('line');
        $response = $oModel->saveRelations($aData);
       
        $oView->displayJSON($response);
    }
    
    public function getLineById() {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id');
        
        $type = "line";
        
        $oView = $this->getView($type, 'json');
        $oModel = $this->getModel($type);
        $response = $oModel->getLineById($id);
        
        $oView->displayJSON($response);
        
    }
    
    public function getFacilityById() {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id');
        
        $type = "facility";
        
        $oView = $this->getView($type, 'json');
        $oModel = $this->getModel($type);
        $response = $oModel->getFacilityById($id);
        
        $oView->displayJSON($response);
        
    }
    
    public function getProgramById() {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id');
        
        $type = "program";
        
        $oView = $this->getView($type, 'json');
        $oModel = $this->getModel($type);
        $response = $oModel->getProgramById($id);
        
        $oView->displayJSON($response);
        
    }
    
    public function getDomainById() {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id');
        
        $type = "domain";
        
        $oView = $this->getView($type, 'json');
        $oModel = $this->getModel($type);
        $response = $oModel->getDomainById($id);
        
        $oView->displayJSON($response);
        
    }
    
    public function getCenterById() {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id');
        
        $type = "center";
        
        $oView = $this->getView($type, 'json');
        $oModel = $this->getModel($type);
        $response = $oModel->getCenterById($id);
        
        $oView->displayJSON($response);
        
    }
    
}

