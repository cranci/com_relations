<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view' );
jimport('joomla.html.pagination');
require_once(JPATH_COMPONENT.'/helpers/centerslist.php');
require_once(JPATH_COMPONENT.'/helpers/facilitieslist.php');
require_once(JPATH_COMPONENT.'/helpers/programslist.php');
require_once(JPATH_COMPONENT.'/helpers/lineslist.php');
require_once(JPATH_COMPONENT.'/helpers/domainslist.php');
require_once(JPATH_COMPONENT.'/helpers/categories.php');

require_once(JPATH_COMPONENT.'/models/relations.php');
/**
 * Events View
 *
 * @since  0.0.1
 */
class RelationsViewRelations extends JViewLegacy
{
	/**
	 * Display the Relations view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
            $linesList = LineslistHelper::getList();
            $this->assignRef('lines', $linesList);
           
            $programsList = ProgramslistHelper::getList();
            $this->assignRef('programs', $programsList);
			
            $centersList = CenterslistHelper::getList();
            $this->assignRef('centers', $centersList);
		
            $domainsList = DomainslistHelper::getList();
            $this->assignRef('domains', $domainsList);
            
           // die(var_dump($domainsList));
            
            $facilitiesList = FacilitieslistHelper::getList();
            $this->assignRef('facilities', $facilitiesList);
            /*
            echo '<pre>'.  print_r($programsList).'</pre>';
            echo '<pre>'.  print_r($facilitiesList).'</pre>';
            */
            // Check for errors.
            if (count($errors = $this->get('Errors')))
            {
                JError::raiseError(500, implode('<br />', $errors));
                return false;
            }
          
        $model = new RelationsModelRelations();
       
        //$model = $this->getModel();
  
        
        $form = $model->getForm();
        /*echo '<pre>'.  print_r($form).'</pre>';
        die();*/
        
        /* $form->setValue('id',null,$profile->id);
        $form->setValue('role',null,$profile->role);
        $form->setValue('website',null,$profile->website);
        $form->setValue('personal_phone',null,$profile->personal_phone);
        $form->setValue('address',null,$profile->address);
        $form->setValue('interest',null,$profile->interests);*/
        
        $this->form = $form;
            
            //$this->addToolBar();
            JHtml::stylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
            JHtml::stylesheet('media/com_relations/css/admin.css');
            JHtml::script('media/com_relations/js/relations.js');
        
            parent::display($tpl);
	}
       
	protected function addToolBar()
	{
            JToolBarHelper::title(JText::_('COM_RELATIONS_NAME'));
            JToolBarHelper::preferences('com_relations', 500, 400);
            
            $user = JFactory::getUser();
            if($user->authorise('core.add','com_relations')) {
                JToolBarHelper::addNew('relations.add');
            }
            if($user->authorise('core.edit','com_relations')) {
                JToolBarHelper::editList('relation.edit');
            }
            if($user->authorise('core.delete','com_relations')) {
                JToolBarHelper::deleteList('', 'relation.delete');
            }
	}
       
        
        

}

