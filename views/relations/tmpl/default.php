<?php
/**
 * @package  com_iitcomponent
 */
// TO DO: Move js code outside php file!!!
// 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
/*
include 'ChromePhp.php';
ChromePhp::log('Hello console!');
*/
require_once(JPATH_COMPONENT.'/helpers/translation.php');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user = JFactory::getUser();
    
$langtag = JFactory::getLanguage()->getTag();
$lng = explode('-', $langtag);
$currentLang = $lng[0];

$jinput = JFactory::getApplication()->input;
$currentComponent = $jinput->get('option');

$params = JComponentHelper::getParams('com_relations');
$ab_url = $params->get('ab_url');
$ab_repo = $params->get('ab_repo', 1);
$ab_images_url = $params->get('ab_images_url');

?>
<form action="index.php?option=com_relations&view=relations" method="post" id="adminForm" name="adminForm">
    <input type="button" class="save-relations" name="save-relations" value="Save relations" id="save-relations" />
    	
    <input type="hidden" name="update-relations-url" class="form-control" id="update-relations-url" value="<?php echo JRoute::_('index.php?option=com_relations&controller=relations&task=updateLinesRelations&format=json') ?>"/>
    <input type="hidden" name="save-url" class="form-control" id="save-url" value="<?php echo JRoute::_('index.php?option=com_relations&controller=relations&task=save&format=json') ?>"/>
    <input type="hidden" name="save-relations-url" class="form-control" id="save-relations-url" value="<?php echo JRoute::_('index.php?option=com_relations&controller=relations&task=updateLinesRelations&format=json') ?>"/>
    <input type="hidden" name="get-line-url" class="form-control" id="get-line-url" value="<?php echo JRoute::_('index.php?option=com_relations&controller=relations&task=getLineById&format=json') ?>"/>
    <input type="hidden" name="get-domain-url" class="form-control" id="get-domain-url" value="<?php echo JRoute::_('index.php?option=com_relations&controller=relations&task=getDomainById&format=json') ?>"/>
    <input type="hidden" name="get-center-url" class="form-control" id="get-center-url" value="<?php echo JRoute::_('index.php?option=com_relations&controller=relations&task=getCenterById&format=json') ?>"/>
    <input type="hidden" name="get-facility-url" class="form-control" id="get-facility-url" value="<?php echo JRoute::_('index.php?option=com_relations&controller=relations&task=getFacilityById&format=json') ?>"/>
    <br/><br/>
    <?php //echo JHtml::_('bootstrap.startTabSet', 'oTab', array('active' => 'linesTab')); ?>
        
    <!-- LINES TAB -->
    <?php //echo JHtml::_('bootstrap.addTab', 'oTab', 'linesTab', JText::_('COM_RELATIONS_LLINES', true)); ?>
    <table class="relations linesBasd">
        <tr>
            <th class="list-box">Lines<br/><input type="button" class="add" name="add-line" value="+" id="add-line" /> </th>
            <th class="list-box">Domains<br/><input type="button" class="add" name="add-domain" value="+" id="add-domain" /> </th>
            <th class="list-box">Centers<br/><input type="button" class="add" name="add-center" value="+" id="add-center" /> </th>
            <th class="list-box">Facilities<br/><input type="button" class="add" name="add-facility" value="+" id="add-facility" /> </th>
        </tr>
        <tr>
            <td class="list-box lines">
                <?php foreach ($this->lines as $key=>$line): ?>
                <div class="checkboxContainer">
                    <!--
                    <input type="checkbox" class="checkline" id="checkline-<?php echo $key ?>" value="<?php echo $key ?>" onclick="onCheckboxAction('line', <?php echo $key ?>, this.value);"/>&nbsp;
                    -->
                    &bull;
                    <!-- 
                    <a id="line-<?php echo $key ?>" class="listItem" href="#" onclick="event.preventDefault(); onItemChange('line', <?php echo $key ?>, lines[<?php echo $key ?>]);">
                    <?php echo $line['name'] ?>
                    </a>
                    -->
                    <span id="line-<?php echo $key ?>"><?php echo $line['name'] ?></span>
                     
                    <a href="#" id="line-edit-btn-<?php echo $key ?>" class="edit-btn" onclick="event.preventDefault(); onItemEdit('line', <?php echo $key ?>)"><i class="fa fa-pencil-square-o"></i></a>
                    <a id="edit-relations-line-<?php echo $key ?>" class="edit-relations-btn"  href="#" onclick="event.preventDefault(); onItemChange('line', <?php echo $key ?>, lines[<?php echo $key ?>]);">
                    <i class="fa fa-code-fork"></i>
                    </a>
                    
                </div>
                
            <?php endforeach; ?>
            </td>
            <td class="list-box domains">
                <?php foreach ($this->domains as $key=>$domain): ?>
                <div class="checkboxContainer"><input type="checkbox" class="checkdomain" id="checkdomain-<?php echo $key ?>" value="<?php echo $key ?>" onclick="onCheckboxAction('domain', <?php echo $key ?>, this.value);"/>&nbsp;
                    <span id="domain-<?php echo $key ?>"><?php echo $domain['name'] ?></span>
                    <a href="#" id="domain-edit-btn-<?php echo $key ?>" class="edit-btn" onclick="event.preventDefault(); onItemEdit('domain', <?php echo $key ?>)"><i class="fa fa-pencil-square-o"></i></a>
                </div>
            <?php endforeach; ?>
            </td>
            <td class="list-box centers">
                <?php foreach ($this->centers as $key=>$center): ?>
                <div class="checkboxContainer"><input type="checkbox" class="checkcenter" id="checkcenter-<?php echo $key ?>" value="<?php echo $key ?>" onclick="onCheckboxAction('center', <?php echo $key ?>, this.value);"/>&nbsp;
                    <span id="center-<?php echo $key ?>"><?php echo $center['name'] ?></span>
                    <a href="#" id="center-edit-btn-<?php echo $key ?>" class="edit-btn" onclick="event.preventDefault(); onItemEdit('center', <?php echo $key ?>)"><i class="fa fa-pencil-square-o"></i></a>
                </div>
            <?php endforeach; ?>
            </td>
             <td class="list-box facilities">
                <?php foreach ($this->facilities as $key=>$facility): ?>
                <div class="checkboxContainer">
                    <!--<input type="checkbox" class="checkfacility" id="checkfacility-<?php echo $key ?>" value="<?php echo $key ?>" onclick="onCheckboxAction('facility', <?php echo $key ?>, this.value);"/>&nbsp;-->
                    <span id="facility-<?php echo $key ?>"><?php echo $facility['name'] ?></span>
                    <a href="#" id="facility-edit-btn-<?php echo $key ?>" class="edit-btn" onclick="event.preventDefault(); onItemEdit('facility', <?php echo $key ?>)"><i class="fa fa-pencil-square-o"></i></a>
                </div>
            <?php endforeach; ?>
            </td>
        </tr>
    </table>
    
    
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalTitle" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
           <div class="modal-column" style="width:15%; ">
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-column" style="width:15%;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="save-new"  >Save</button>
            </div>
            <div class="modal-column" style="width:40%;">
                <div class="user-message" id="user-message"></div>
            </div>
            <div class="modal-column" style="width:30%; ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"  style="float:right;"><span aria-hidden="true">&times;</span></button>
            </div>
          
        
      </div>
      <div class="modal-body"  >
            
        <div class="fields-group" id="line-domain-fields">
            
            <div class="modal-row" style="display: table; width: 100%">
            <div class="modal-column" style="display: table-cell; width: 50%">
                
                <?php foreach ($this->form->getFieldset('texareas') as $field): ?>
            		<div class="form-group">
                   	<?php echo $field->label; ?>
                   	<?php echo $field->input; ?>
                   	</div>
                <?php endforeach; ?>
                
                <!--
                <div class="form-group">
                    <label for="name-en" class="control-label">Name (En):</label>
                    <input type="text" required name="name-en" class="form-control" id="name-en"/>
                </div>
                <div class="form-group">
                    <label for="name-it" class="control-label">Name (It):</label>
                    <input type="text" required name="name-it" class="form-control" id="name-it" />
                </div>
                <div class="form-group">
                    <label for="alias-en" class="control-label">Alias (En):</label>
                    <input type="text" required name="alias-en" class="form-control" id="alias-en"/>
                </div>
                <div class="form-group">
                    <label for="alias-it" class="control-label">Alias (It):</label>
                    <input type="text" required name="alias-it" class="form-control" id="alias-it"/>
                </div>
                <div class="form-group">
                    <label for="description-en" class="control-label">Description (En):</label>
                    <textarea cols="80" rows="8" name="description-en" class="form-control" id="description-en"></textarea>
                    
                </div>
                <div class="form-group">
                    <label for="description-it" class="control-label">Description (It):</label>
                    <textarea cols="80" rows="8" name="description-it" class="form-control" id="description-it"></textarea>
                </div> 
                -->
            </div>
            <div class="modal-column" style="display: table-cell; width: 50%">
                <image src="" id="image"/>
                <br/><br/>
                <div class="input-group">
                    <span class="input-group-btn">
                        <a href="#" id="ab-button" class="btn btn-primary">Add/Change image</a>
                    </span>
                </div> 
            </div>
            
            </div>
            <div class="modal-row">
            <?php foreach ($this->form->getFieldset('editors') as $field): ?>
            		<div class="form-group">
                   	<?php echo $field->label; ?>
                   	<?php echo $field->input; ?>
                   	</div>
                <?php endforeach; ?>
            </div>
            <!--
            <div class="modal-column">
                <image src="" id="image"/>
                <br/><br/>
                <div class="input-group">
                    <span class="input-group-btn">
                        <a href="#" id="ab-button" class="btn btn-primary">Add/Change image</a>
                    </span>
                </div> 
            </div>
            -->
        </div>
        <div class="fields-group" id="center-fields">
            
            <div class="modal-column">
                <div class="form-group">
                    <label for="name" class="control-label">Name:</label>
                    <input type="text" required name="name" class="form-control" id="name"/>
                </div>
                <div class="form-group">
                    <label for="shortcut" class="control-label">Shortcut:</label>
                    <input type="text" required name="name" class="form-control" id="shortcut"/>
                </div>
                <div class="form-group">
                    <label for="alias" class="control-label">Alias:</label>
                    <input type="text" required name="name" class="form-control" id="alias"/>
                </div>
                <div class="form-group">
                    <label for="country" class="control-label">Country:</label>
                    <input type="text"  name="country" class="form-control" id="country"/>
                </div>
                <div class="form-group">
                    <label for="city" class="control-label">City:</label>
                    <input type="text"  name="city" class="form-control" id="city"/>
                </div> 
            </div> 
            
            <div class="modal-column">
                <div class="form-group">
                    <label for="address" class="control-label">Address:</label>
                    <input type="text"  name="address" class="form-control" id="address"/>
                </div>
                <div class="form-group">
                    <label for="zip" class="control-label">Zip/Cap:</label>
                    <input type="text"  name="zip" class="form-control" id="zip"/>
                </div>
                <div class="form-group">
                    <label for="phone" class="control-label">Phone:</label>
                    <input type="text"  name="phone" class="form-control" id="phone"/>
                </div>
                <div class="form-group">
                    <label for="lat" class="control-label">Latitude:</label>
                    <input type="text"  name="lat" class="form-control" id="lat"/>
                </div>
                <div class="form-group">
                    <label for="lng" class="control-label">Longitude:</label>
                    <input type="text"  name="lng" class="form-control" id="lng"/>
                </div>
            </div>        
                
        </div>
            
            <input type="hidden" name="id" class="form-control" id="id"/>
            <input type="hidden" name="type" class="form-control" id="type"/>
       
      </div>
        <!--
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary" id="save-new"  >
              Save
          </button>
        </div>--> <!-- end footer -->
    </div>
  </div>
</div>
    <?php echo JHtml::_('form.token'); ?>
</form>

<script>
    
    //var eGlobalMessage = document.getElementById('user-message');
    var eGlobalMessage = '';
    var currentCategory = '';
    var currentAction = '';
    
    
    var lines = <?php echo json_encode($this->lines); ?> 
    var domains = <?php echo json_encode($this->domains); ?> 
    var centers = <?php echo json_encode($this->centers); ?> 
    var facilities = <?php echo json_encode($this->facilities); ?> 
    
    var linesChanges = new Array('domains', 'centers', 'facilities');
    linesChanges['domains'] = new Array();
    linesChanges['centers'] = new Array();
    linesChanges['facilities'] = new Array();
    var domainsChanges = new Array();
    var centersChanges = new Array();
    var facilitiesChanges = new Array();
    

    jQuery( document ).ready(function() 
    {  
        init(lines, domains, centers, facilities);
        
        jQuery('#addModal').hide();
        jQuery('body').removeClass('modal-open');
        jQuery('.modal-backdrop').remove();
        
        updateRelationsUrl(jQuery('#update-relations-url').val());
        updateAddNewUrl(jQuery('#save-url').val());
      });
     

    jQuery('.add').click(function( event ) 
    {
        
        var thisId = jQuery(this).prop('id');
        
        var arr = thisId.split('-');
        
        currentCategory = arr[1];
        currentAction = 'add';
       
        var modal = jQuery('#addModal');
        initForm(currentAction, currentCategory);
        modal.modal('show');
    });
    
    function initForm(action, category)
    {
        jQuery('#index-alert').remove();
        
        if(category=='center'){
            jQuery('#center-fields').show();
            jQuery('#line-domain-fields').hide();
        }
        else
        {
            jQuery('#center-fields').hide();
            jQuery('#line-domain-fields').show();
        }
        
        var label = action+' '+category;
        
        jQuery(".modal-body input[type='text']").val('');
        jQuery('#modalTitle').html(label.toUpperCase());
        jQuery('#type').val(category);
    }
    
    jQuery('#save-new').on('click', function() 
    {
        var newData = {};
        newData['type'] = jQuery('#type').val();
        
        if(jQuery('#type').val() == 'center')
        {
            newData['id'] = jQuery('#id').val();
            newData['name'] = jQuery('#name').val();
            newData['shortcut'] = jQuery('#shortcut').val();
            newData['alias'] = jQuery('#alias').val();
            newData['country'] = jQuery('#country').val();
            newData['city'] = jQuery('#city').val();
            newData['address'] = jQuery('#address').val();
            newData['zip'] = jQuery('#zip').val();
            newData['phone'] = jQuery('#phone').val();
            newData['lat'] = jQuery('#lat').val();
            newData['lng'] = jQuery('#lng').val();
            newData['state'] = jQuery('#state').val();
        }
        else
        {
            newData['id'] = jQuery('#id').val();
            newData['state'] = jQuery('#state').val();
            newData['name_en'] = jQuery("input[name*='name-en']").val();
            newData['name_it'] = jQuery("input[name*='name-it']").val();
            newData['alias_en'] = jQuery("input[name*='alias-en']").val();
            newData['alias_it'] = jQuery("input[name*='alias-it']").val();
            
            var desc_en = jQuery(tinyMCE.get('description_en').getBody()).html();
            var desc_it = jQuery(tinyMCE.get('description_it').getBody()).html();
            
            newData['description_en'] = encodeURI(desc_en);
            newData['description_it'] = encodeURI(desc_it);
            //newData['description_en'] = jQuery(tinyMCE.get('description_en').getBody()).html();
            //newData['description_it'] = jQuery(tinyMCE.get('description_it').getBody()).html();
            
            newData['img_source'] = jQuery('#image').attr('src');
        }
        
        jQuery('#index-alert').remove();
            saveData(addNewUrl, newData);
          
       
    });
    
    
    function saveData(saveUrl, newData) {
       
        okMessage = 'Data has been saved';
        
        console.log( 'newdata:' );
        console.log( newData );
        
        return jQuery.ajax({
            url: saveUrl,
            //contentType: 'application/json; charset=utf-8',
            data: newData,
            async:true,
            type:'post'
        })
        .done(function(response) {
            
            //console.log(response);
            if(response.done === true) 
            {
                console.log('***data back***');
                console.log(response.data);
                
                if(newData['id'] !== ''){
                    updateItem(response.data);
                }
                
                setAlert(eGlobalMessage, 'success', okMessage, 5000);
            }
            else  {
                //console.log('after send:');
                //console.log(response.data);
                setAlert(eGlobalMessage, 'danger', 'Error saving data. Reason: ' + response.data);
                return false;
            }
        })
        .fail(function(e) {
            
            setAlert(eGlobalMessage, 'danger', 'Error saving data ('+ e.statusText +')');
            return false;
        });
        
        
    }
    
    jQuery('#save-relations').on('click', function() 
    {
        var url = jQuery('#save-relations-url').val();
       
        saveRelations(url);
    });
    
    function setAlert(eMessage, type, message, timeout) {
        var timeout = 3000;
        
        type = 'alert-'+type;
    
   var messageTemplate = '<div id="index-alert" class="alert '+type+'  alert-dismissable" style="width:80%; margin-left: 10px; opacity: 1;">'+message+'</div>';
 
    if(message !== ''){
    //jQuery("body").append( messageTemplate );
    jQuery("#user-message").append( messageTemplate );
    
    }
   /*
        if(timeout > 0) {
            setTimeout(function() {
                jQuery('#index-alert').hide( "slow", function() {
                    
                  });
                
                
            }, timeout);
        }
        */
    }
    
    function removeAlert(eMessage) {
        jQuery(eMessage).find('.alert').alert('close');
    }
    
    
</script>

<!-- ASSETBANK STUFF -->
<script> 
  
  var repoNumber = '<?php echo $ab_repo ?>';
  var imgUrl = '<?php echo $ab_images_url?>';//"https://multimedia.iit.it/cms-images/";
  var abUrl = '<?php echo $ab_url ?>'; //"https://multimedia.iit.it/asset-bank/";

  var abCallbackUrl = '<?php print(JURI::base().'components/'.$currentComponent.'/library/getimagedata.php') ?>';
  
  var ABWindow;
  var imagesUrl;
  
  jQuery('#ab-button').on('click', function() {
        event.preventDefault();
        openABWindowForMedia(abCallbackUrl, repoNumber, abUrl, imgUrl);
    });
  
  
  function openABWindowForMedia(callbackUrl, repoNumber, abUrl, imgUrl)
  {
      if(!imgUrl || imgUrl==="" || imgUrl===undefined)
          imgUrl = "https://multimedia.iit.it/cms-images/";
      
      if(!abUrl || abUrl==="" || abUrl===undefined)
          abUrl = "https://multimedia.iit.it/asset-bank/";
      
      if(!repoNumber || repoNumber==="" || repoNumber===undefined)
          repoNumber = "2";
      /*
      if(!callbackUrl || callbackUrl=="" || callbackUrl==undefined)
          callbackUrl = "2";*/
      
      if(!callbackUrl || !repoNumber)
          return false;
      
      
      imagesUrl = imgUrl; 
      
        var completeUrl = abUrl+"action/selectImageForCms?repositoryNumber="+repoNumber+"&callbackurl="+callbackUrl;
       
        // open Asset Bank popup window (passing in username and any other parameters)
	ABWindow=window.open(completeUrl,'ABPopup','height=800,width=1024,resizable=yes,status=yes,scrollbars=yes');
	// give new popup window the focus if browser supports it
	if (window.focus) {ABWindow.focus();}
  }

 function insertMediaImage(xmlString)
{
    var xmlDoc = jQuery.parseXML(xmlString);
    var $xml = jQuery(xmlDoc);
    var $url = $xml.find('url');
    var $title = $xml.find('title');
    var $width = $xml.find('width');
    var $height = $xml.find('height');
    
    var urlArray = $url.text().split("/");
    var actualUrl = imagesUrl+urlArray[urlArray.length-1];
    
    jQuery('#image').show();
    jQuery('#image').attr('src', actualUrl);
    
    jQuery('#ab-button').html('Change media');
     
    ABWindow.close();
}
        
</script>

