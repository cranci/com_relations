<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Events View
 *
 * @since  0.0.1
 */
jimport( 'joomla.application.component.view' );
jimport('joomla.html.pagination');
class RelationsViewRelation extends JViewLegacy
{
        protected $form = null;
	protected $item;
	protected $state;
    
	function display($tpl = null)
	{
            // Get data from the model
            $this->form = $this->get('Form');
            
            //die(var_dump($this->form));
            
            $this->item = $this->get('Item');
            $oModel = $this->getModel();
            
            $lines = $oModel->getLines();
            $programs = $oModel->getPrograms();
            $centers = $oModel->getCenters();
            $domains = $oModel->getDomains();
            
            $params = JComponentHelper::getParams('com_relations');
            $this->assignRef('lines', $lines);
            $this->assignRef('programs', $programs);
            $this->assignRef('centers', $centers);
            $this->assignRef('domains', $domains);
         
            // Check for errors.
            if (count($errors = $this->get('Errors')))
            {
                JError::raiseError(500, implode('<br />', $errors));
                return false;
            }

            $this->addToolBar();
            JHtml::_('jquery.framework');
            //JHtml::stylesheet('media/com_events/css/admin.css');
            //JHtml::script('media/com_events/js/mustache.min.js');
           
            // Display the template
            parent::display($tpl);
	}
        
    function edit($tpl = null)
	{
            die("ciu");
    }
        
        /**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
            JToolBarHelper::title(JText::_('COM_RELATIONS_NAME'));
            
            $input = JFactory::getApplication()->input;
 
            // Hide Joomla Administrator Main menu
            $input->set('hidemainmenu', true);

            $isNew = ($this->item->id == 0);
/*
            if ($isNew)
            {
                $title = JText::_('COM_EVENTS_NEW_EVENT');
            }
            else
            {
                $title = JText::_('COM_EVENTS_EDIT_EVENT');
            }*/

            JToolBarHelper::title($title, 'relation');
            JToolBarHelper::save('relation.save');
            JToolBarHelper::cancel(
                'relation.cancel',
                $isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE'
            );
	}

}

