<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Events View
 *
 * @since  0.0.1
 */
class RelationsViewCenter extends JViewLegacy
{
    public function __counstruct() 
    {
        parent::__construct();
    }
    
    public function displayJSON($response, $tpl = null) 
    {
        //echo 'fdghfdasdas ' . $response; die();
        $this->addTemplatePath(JPATH_COMPONENT_ADMINISTRATOR.'/views/center/json');
        $this->setLayout('display.json');
        
        $this->assignRef('aResponse', $response);
        
        parent::display($tpl);
    }
}