<?php
/*
 * Trusted private constant existence check to avoid direct execution of the code of the file
 */
defined('_JEXEC') or die('Restricted access');
 
// Get the document object.
//$document =& JFactory::getDocument();
 
// Set the MIME type for JSON output.
//$document->setMimeEncoding('application/json');

echo json_encode($this->aResponse);