<?php
defined('_JEXEC') or die("Programs");
jimport('joomla.form.formfield');

class DomainslistHelper 
{

    public function getList()
    {
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/domains';
                
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        $output = array();
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            foreach ($raw->_embedded->domain as $obj)
            {
                $relatedLines = self::getRelatedLines($obj->id);
                //$relatedCenters = self::getRelatedCenters($line->id);
               
                $output[$obj->id] = array("id" =>$obj->id, "name" => $obj->name_en, "lines" => $relatedLines, "centers" => null);// n on sono previsti centri
                //array_push($output, array("id" => $program->id, "name" => $program->name_en));
                //$output[$program->id] = $program->name_en;
            }
        }
        return $output;
        
    }

    public static function getDomainById($id)
    {
        if(!$id)
        {return null;}
        
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/domains/'.$id;
        
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            
            return $raw;
        }
        
        return null;
        
    }

public function getRelatedLines($id){
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/domain/domainlines?program_id='.$id;
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        $output = array();
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            if($raw->total_items == 0)
            {
                return null;
            }
            else
            {
                
                foreach ($raw->_embedded->domainlines as $domainlines)
                {
                    array_push($output, array("id" => $domainlines->id, "name" => $domainlines->name_en));
                }
            }
            
        }
		return $output;
        
    }

    
}
