<?php
defined('_JEXEC') or die("Programs");
jimport('joomla.form.formfield');

class ProgramslistHelper 
{

    public function getList()
    {
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/programs';
                
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        $output = array();
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            foreach ($raw->_embedded->program as $program)
            {
                $relatedLines = self::getRelatedLines($program->id);
                //$relatedCenters = self::getRelatedCenters($line->id);
               
                $output[$program->id] = array("id" =>$program->id, "name" => $program->name_en, "lines" => $relatedLines, "centers" => null);// n on sono previsti centri
                //array_push($output, array("id" => $program->id, "name" => $program->name_en));
                //$output[$program->id] = $program->name_en;
            }
        }
        return $output;
        
    }

    public static function getProgramById($id)
    {
        if(!$id)
        {return null;}
        
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/programs/'.$id;
        
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            
            return $raw;
        }
        
        return null;
        
    }

public function getRelatedLines($programid){
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/program/programlines?program_id='.$programid;
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        $output = array();
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            if($raw->total_items == 0)
            {
                return null;
            }
            else
            {
                
                foreach ($raw->_embedded->programlines as $programlines)
                {
                    array_push($output, array("id" => $programlines->id, "name" => $programlines->name_en));
                }
            }
            
        }
		return $output;
        
    }

    
}
