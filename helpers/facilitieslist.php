<?php
defined('_JEXEC') or die("centers");
jimport('joomla.form.formfield');

class FacilitieslistHelper 
{
    public function getList()
    {
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/facilities';
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        $output = array();
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            foreach ($raw->_embedded->facility as $facility)
            {
                $relatedLines = self::getRelatedLines($facility->id);
                $output[$facility->id] = array("id" =>$facility->id, "name" => $facility->name_en, "lines" => $relatedLines);
            }
        }
       
	return $output;
    }
    
    public static function getFacilityById($id)
    {
        if(!$id)
        {return null;}
        
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/facilities/'.$id;
       
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            
            return $raw;
        }
        
        return null;
    }
    
    public function getRelatedLines($id){
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/facility/facilitylines?facility_id='.$id;
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        $output = array();
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            if($raw->total_items == 0)
            {
                return null;
            }
            else
            {
                
                foreach ($raw->_embedded->centerlines as $facilitylines)
                {
                    array_push($output, array("id" => $facilitylines->id, "name" => $facilitylines->name.' ('.$facilitylines->shortcut.')'));
                }
            }
            
        }
	return $output;
    }
    
    
    
    
    
    
    
    
    
    
    
}
