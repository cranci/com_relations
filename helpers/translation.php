<?php
/**
 * @package  com_openings
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_COMPONENT.'/helpers/database.php');

class TranslationHelper 
{
    public static function getDefaultLanguage() 
    {
        $langtag = JFactory::getLanguage()->getTag();
        $val = explode('-', $langtag);
        return $val[0];
    }
    
    public static function getOtherLanguage($lang)
    {
        return ($lang == 'en') ? 'it' : 'en';
    }
    
    public static function getDefaultTranslation($field, $oRow, $lang = null) 
    {
        if ($lang == null) {
            $lang = self::getDefaultLanguage();
        }
        
        $fieldValue = self::getTranslatedField($field, $oRow, $lang);
        
        if ( $fieldValue == null ) {
            
            $fieldValue = self::getTranslatedField($field, $oRow, self::getOtherLanguage($lang));
        }
        
        return $fieldValue;
    }
    
    public static function getTranslatedField($field, $oRow, $lang) {
        $fieldKey = $field . '_' . $lang;
        if ( isset($oRow->$fieldKey) ) {
            return $oRow->$fieldKey;
        }
        else {
            return null;
        }
    }
    
    // Database functions
    
    /**
     * Method to save translatable field to the translation table.
     *
     * @param string $tableName  Name of the connected table (used to check for existing translations)
     * @param string $fieldName  Name of the field that we are saving f.e alias, description, etc.  
     * @param array  $data  Data to save.
     *
     * @return  int    New / updated translation id
     *
     */
    public static function saveTranslation($tableName, $fieldName, $data)
    {
        $result = 0;
        
        if(!isset($data['id']) || $data['id'] == 0) 
        {
            $result = self::insertTranslation($fieldName, $data);
        }
        else
        {
            $translationId = self::findTranslationId($tableName, $fieldName, $data['id']);
            if ($translationId > 0) {
                $result = self::updateTranslation($fieldName, $translationId, $data);
            }
            else {
                $result = self::insertTranslation($fieldName, $data);
            }
        }
        
        return $result;
    }
    
    /**
     * Inserts the row into translation table
     *
     * @param string $fieldName  Name of the field that we are updating (used to select automatically right elements from data array) f.e alias, description, etc. 
     * @param array  $data  Data to save.
     * 
     * @return  int    New row id on success, 0 otherwise.
     *
     */
    public static function insertTranslation($fieldName, $data)
    {
        $result = 0;
        
        $db = DatabaseHelper::getApiDb();
        $columns = array('en', 'it');
        $values = array($db->quote($data[$fieldName.'_en'], true), $db->quote($data[$fieldName.'_it'], true));

        $query = $db->getQuery(true);
        $query->insert( $db->quoteName('translation') );
        $query->columns( $db->quoteName($columns) );
        $query->values( implode(',', $values) );
        $db->setQuery($query);
        $db->execute();
        $result = $db->insertid();
        
        return $result;
    }
    
    /**
     * Updates the translation row
     *
     * @param string $fieldName  Name of the field that we are updating (used to select automatically right elements from data array) f.e alias, description, etc.  
     * @param int  $translationId  Id of the translation that we are updating.
     * @param array  $data  Data to save.
     * 
     * @return  int    Translation id
     */
    public static function updateTranslation($fieldName, $translationId, $data)
    {
        $db = DatabaseHelper::getApiDb();
        $query = $db->getQuery(true);
        $fields = array(
            $db->quoteName('en') . ' = ' . $db->quote($data[$fieldName.'_en']),
            $db->quoteName('it') . ' = ' . $db->quote($data[$fieldName.'_it'])
        );
        $condition = $db->quoteName('id') . ' = ' . (int)$translationId;
        $query->update($db->quoteName('translation'));
        $query->set($fields);
        $query->where($condition);
        $db->setQuery($query);
        
        $db->execute();
        
        return $translationId;
    }
    
    /**
     * Checks if translation for the specified field name exists in the given table for the specific row
     *
     * @param string $tableName  Name of the connected table (used to check for existing translations).
     * @param string $fieldName  Name of the field that we are looking for f.e alias, description, etc.  
     * @param int  $rowId  Id of the table row that we are checking.
     *
     * @return  int    An id of the row if exists, value of 0 otherwise.
     *
     */
    public static function findTranslationId($tableName, $fieldName, $rowId)
    {
        if ($rowId <= 0 || $tableName == null) {
            return 0;
        }
        
        $db = DatabaseHelper::getApiDb();
        $query = $db->getQuery(true);
        $query->select($db->quoteName($fieldName.'_tid'));
        $query->from($db->quoteName($tableName));
        $query->where($db->quoteName('id') . ' = ' . (int)$rowId);
        $db->setQuery($query);
        $db->execute();
        $result = $db->loadColumn();
        
        return isset($result[0]) ? $result[0] : 0;
    }
    
    
}

