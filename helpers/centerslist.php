<?php
defined('_JEXEC') or die("centers");
jimport('joomla.form.formfield');

class CenterslistHelper 
{
    public function getList()
    {
        $session = JFactory::getSession();
        $options = array();
        
        $attr = '';
        $attr .= ' onchange="onCenterChanged(this);"' ;
        
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/centers';
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        $output = array();
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            
            
            foreach ($raw->_embedded->center as $center)
            {
                $relatedLines = self::getRelatedLines($center->id);
                $output[$center->id] = array("id" =>$center->id, "name" => $center->name, "lines" => $relatedLines, "programs" => null);// non sono previsti programmi
                //array_push($output, array("id" => $center->id, "name" => $center->name));  
                //$output[$center->id] = $center->name.' ('.$center->shortcut.')';
            }
        }
        
		return $output;
        
    }
    
    public static function getCenterById($id)
    {
        if(!$id)
        {return null;}
        
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/centers/'.$id;
        
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            
            return $raw;
        }
        
        return null;
        
    }
    
    public function getRelatedLines($centerid){
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/center/centerlines?center_id='.$centerid;
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        $output = array();
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            if($raw->total_items == 0)
            {
                return null;
            }
            else
            {
                
                foreach ($raw->_embedded->centerlines as $centerlines)
                {
                    array_push($output, array("id" => $centerlines->id, "name" => $centerlines->name.' ('.$centerlines->shortcut.')'));
                }
            }
            
        }
		return $output;
        
    }
    
    
    
    
    
    
    
    
    
    
    
}
