<?php
/**
 * @package  com_iitcomponent
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class DatabaseHelper 
{
    
    public static function getApplicationDb()
    {
        $oApplication = JFactory::getApplication();
        $aDbConfig = array();
        
        $aDbConfig['driver'] = $oApplication->getCfg('dbtype');
        $aDbConfig['host'] = $oApplication->getCfg('host');
        $aDbConfig['user'] = $oApplication->getCfg('user');
        $aDbConfig['password'] = $oApplication->getCfg('password');
        $aDbConfig['database'] = $oApplication->getCfg('db');
        $aDbConfig['prefix'] = $oApplication->getCfg('dbprefix');
        return JDatabase::getInstance($aDbConfig); 
    }
    
    public static function getApiDbConfig()
    {
        $oApplication = JFactory::getApplication();
        
        $aApiDbConfig = array();
        $aApiDbConfig['driver'] = $oApplication->getCfg('dbtype');
        $aApiDbConfig['host'] = $oApplication->getCfg('api_host_db');
        $aApiDbConfig['user'] = $oApplication->getCfg('api_user');
        $aApiDbConfig['password'] = $oApplication->getCfg('api_password');
        $aApiDbConfig['database'] = $oApplication->getCfg('api_db');
        $aApiDbConfig['prefix'] = $oApplication->getCfg('api_dbprefix');
        return $aApiDbConfig;
    }
    
    public static function getApiDb()
    {
        $aApiDbConfig = self::getApiDbConfig();
        return JDatabase::getInstance($aApiDbConfig);
    }
    
    
    public static function getTranslatedFields ($sTable, $aFields, $aLang, $id) 
    {
        $result = array();
        $aSelectWhat = array();
        $aJoin = array();
        $index = 0;
        
        $apiDb = self::getApiDb();
        
        foreach ($aFields as $field => $as) {
            foreach($aLang as $lang) {
                array_push($aSelectWhat, " t$index.$lang AS $as"."_$lang");
            }
            array_push($aJoin, $apiDb->quoteName('translation')." AS t$index ON $sTable.$field = t$index.id ");
            $index++;
        }
        
        $query = $apiDb->getQuery(true);
        
        $query->select(implode(', ', $aSelectWhat));
        $query->from($apiDb->quoteName($sTable));
        foreach($aJoin as $join) {
            $query->join('LEFT', $join);
        }
        $query->where("$sTable.id = $id");
        
        //var_dump($query);
        
        $apiDb->setQuery($query);
        
        try
        {
            $result = $apiDb->loadAssoc();
        }
        catch (RuntimeException $e)
        {
            JError::raiseWarning(500, $e->getMessage());
        }
        
        return $result;
    }
}

