<?php
defined('_JEXEC') or die("Lines");
jimport('joomla.form.formfield');

class LineslistHelper 
{
    public function getList()
    {
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/lines';
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
          
        $output = array();
        //$output[0] = "Select...";
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            foreach ($raw->_embedded->line as $line)
            {
                $relatedDomains = self::getRelatedDomains($line->id);
                $relatedPrograms = self::getRelatedPrograms($line->id);
                $relatedCenters = self::getRelatedCenters($line->id);
                //$output[$line->id] = $line->name_en;
                $output[$line->id] = array("id" =>$line->id, "name" => $line->name_en, "programs" => $relatedPrograms, "centers" => $relatedCenters, "domains" => $relatedDomains);
            }
        }
        /*
        echo '<pre>';
        print_r($output);
        echo '</pre>';
            die();*/
		return $output;
        
    }
    
    public static function getLineById($id)
    {
        
        if(!$id)
        {return null;}
        
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/lines/'.$id;
        
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        //return $api_url;
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            
            return $raw;
        }
        
        return null;
        
    }
    
    public function getRelatedDomains($lineid){
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/line/linedomains?line_id='.$lineid;
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        $output = array();
        //$output[0] = "Select...";
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            if($raw->total_items == 0)
            {
                return null;
            }
            else
            {
                foreach ($raw->_embedded->linedomains as $linedomain)
                {
                    array_push($output, array("id" => $linedomain->id, "name" => $linedomain->name_en));
                }
            }
            
        }
       // die(var_dump($output));
		return $output;
        
    }
    
    public function getRelatedPrograms($lineid){
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/line/lineprograms?line_id='.$lineid;
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
        $output = array();
        //$output[0] = "Select...";
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            if($raw->total_items == 0)
            {
                return null;
            }
            else
            {
                
                foreach ($raw->_embedded->lineprograms as $lineprogram)
                {
                    array_push($output, array("id" => $lineprogram->id, "name" => $lineprogram->name_en));
                //$output[$lineprogram->id] = $lineprogram->name_en;
                }
            }
            
        }
        //die(var_dump($output));
		return $output;
        
    }
    
    
    public function getRelatedCenters($lineid){
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/line/linecenters?line_id='.$lineid;
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
          
        $output = array();
        //$output[0] = "Select...";
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            if($raw->total_items == 0)
            {
                return null;
            }
            else
            {
                foreach ($raw->_embedded->linecenters as $linecenter)
                {
                    array_push($output, array("id" => $linecenter->id, "name" => $linecenter->name));  
                    //$output[$linecenter->id] = $linecenter->name;
                }
            }
            
            
            
        }
        //die(var_dump($output));
		return $output;
    }
    
}
