<?php
defined('_JEXEC') or die("Lines");
jimport('joomla.application.categories');

class CategoriesHelper 
{
    
    public function __construct($options = array())
	{
            $options['table'] = '#__content';
            $options['extension'] = 'com_relations';
            parent::__construct($options);
	}

        
        
        public function getItems($recursive = false)
	{
		if (!count($this->_items)) {
			

			$app = JFactory::getApplication();
			
			$options = array();
			$options['countItems'] = 20;

//$categories = JCategories::getInstance('Content', $options);

			$categories = JCategories::getInstance('com_content', $options);

			$this->_parent = $categories->get('root');

			if (is_object($this->_parent)) {
				$this->_items = $this->_parent->getChildren($recursive);
			}
			else {
				$this->_items = false;
			}
		}

		return $this->_items;
	}
        
        
}

