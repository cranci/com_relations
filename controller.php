<?php
/**
 * @package  com_iitcomponent
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * General Controller of relations
 *
 * @package  com_relations
 */
class RelationsController extends JControllerLegacy
{
    protected $default_view = 'relations';
    
    public function __construct($config = array())
	{
        
		parent::__construct($config);
                
	}
        
        
        public function display($cachable = false, $urlparams = false)
        {
            
            $vName = $this->input->get('view', 'relations');

		$document = JFactory::getDocument();
		$vType    = $document->getType();

                $vLayout = $this->input->get('layout', 'default', 'string');
                
		// Get/Create the view
		$view = $this->getView($vName, $vType);
		$view->addTemplatePath(JPATH_COMPONENT_ADMINISTRATOR . '/views/' . strtolower($vName) . '/tmpl');

		// Get/Create the model
		/*
                 if ($model = $this->getModel('relations'))
		{
			// Push the model into the view (as default)
			$view->setModel($model, true);
		}
                */

		// Set the layout
		$view->setLayout($vLayout);

		// Display the view
		$view->display();
                
		return $this;
        }
	
	
}