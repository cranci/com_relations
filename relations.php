<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Get an instance of the controller prefixed by Relations
$controller = JControllerLegacy::getInstance('Relations');

// Perform the Request task
$input = JFactory::getApplication()->input;

// if controller is set - load it (work around for format=json not working with task=controllername.function)
$sController = $input->getWord('controller', null);




if($sController != null) {
    $sPath = JPATH_COMPONENT_ADMINISTRATOR.'/controllers/'.$sController.'.php';
    
    if (file_exists($sPath)) 
    {
        require_once($sPath);
        $sClassName = 'RelationsController'.$sController;
        $controller = new $sClassName();
    }
}

$controller->execute($input->getCmd('task'));
// Redirect if set by the controller
$controller->redirect();
