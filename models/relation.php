<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_COMPONENT.'/helpers/database.php');
require_once(JPATH_COMPONENT.'/helpers/translation.php');
require_once(JPATH_COMPONENT.'/helpers/eventtags.php');

class RelationsModelRelation extends JModelAdmin
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function getTable($type = 'Events', $prefix = 'EventsTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }
    
    /**
     * Method to get the record form.
     *
     * @param   array    $data      Data for the form.
     * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
     *
     * @return  mixed    A JForm object on success, false on failure
     *
     * @since   1.6
     */
    public function getForm($data = array(), $loadData = true)
    {
            // Get the form.
            $form = $this->loadForm(
                'com_events.event',
                'event',
                array(
                    'control' => 'jform',
                    'load_data' => $loadData
                )
            );
            
            
            if (empty($form))
            {
                return false;
            }

            return $form;
    }
    
    /**
     * Method to get the data that should be injected in the form.
     *
     * @return  mixed  The data for the form.
     *
     * @since   1.6
     */
    protected function loadFormData()
    {
            // Check the session for previously entered form data.
            $data = JFactory::getApplication()->getUserState(
                'com_events.edit.event.data',
                array()
            );
            
            if (empty($data))
            {
                $data = $this->getItem();
            }
            else
                $data = Joomla\Utilities\ArrayHelper::toObject($data, 'JObject');
  
            if(isset($data->id)) {
                
                $tfields = array(
                    'alias_tid' => 'alias',
                    'name_tid'=> 'name',
                    'description_tid' => 'description'
                );
                $translatedData = DatabaseHelper::getTranslatedFields("events", $tfields, array('en', 'it'), $data->id);
            }
           
            $data->setProperties($translatedData);
            
            $speakers = $this->getEventSpeakers($data->id);
            for($i = 0; $i < count($speakers) ; $i++) 
            {
                $speakers[$i] = $speakers[$i]['id'];
            }
            $data->set('speakers', implode(',', $speakers));
            
            $lines = $this->getEventLines($data->id);
            for($i = 0; $i < count($lines) ; $i++) 
            {
                $lines[$i] = $lines[$i]['id'];
            }
            
            
            $tags = implode(',', $this->getEventTags($data->id));
            $data->set('tags', $tags);
            
            $data->set('lines', $lines);
            
            return $data;
    }
    
    
    public function getAllEvents() {
        $apiDb = DatabaseHelper::getApiDb();
        $query = $apiDb->getQuery(true);
        $query->select('`c`.*');
        $query->from($apiDb->quoteName('events', 'c'));
        
        $apiDb->setQuery($query);
        
        try
        {
            $result = $apiDb->loadAssocList();
        }
        catch (RuntimeException $e)
        {
            JError::raiseWarning(500, $e->getMessage());
        }
        
        return $result;
    }
    
    public function getEventById($id) {
        $apiDb = DatabaseHelper::getApiDb();
        $query = $apiDb->getQuery(true);
        $query->select('`c`.*');
        $query->from($apiDb->quoteName('events', 'c'));
        $query->where($apiDb->quoteName('id') . ' = ' . $id);
        $apiDb->setQuery($query);
        
        try
        {
            $result = $apiDb->loadRow();
        }
        catch (RuntimeException $e)
        {
            JError::raiseWarning(500, $e->getMessage());
        }
        
        return $result;
    }
    
    public function getEventMedia($id)
    {
        if($id <= 0) {
            //JError::raiseWarning(500, "Id must be a postive number");
            return array();
        }
        $result = array();
        
        $apiDb = DatabaseHelper::getApiDb();
        $query = $apiDb->getQuery(true)
        ->select('*')
        ->from($apiDb->quoteName('event_media'))
       ->where($apiDb->quoteName('event_id') . ' = '.(int)$id);
        
        $apiDb->setQuery($query);
        
        try
        {
            $result = $apiDb->loadAssocList();
        }
        catch (RuntimeException $e)
        {
            JError::raiseWarning(500, $e->getMessage());
        }
        
        return $result;
    }
    
    public function getEventTags($id)
    {
        if($id <= 0) {
            //JError::raiseWarning(500, "Id must be a postive number");
            return array();
        }
        $result = array();
        
        $apiDb = DatabaseHelper::getApiDb();
        $query = $apiDb->getQuery(true)
        ->select('name')
        ->from($apiDb->quoteName('tags', 's'))
        ->join('LEFT', $apiDb->quoteName('events_tags', 'es') . ' ON es.tag_id = s.id ')
        ->where($apiDb->quoteName('es.event_id') . ' = '.(int)$id);
        
        $apiDb->setQuery($query);
        
        try
        {
            $result = $apiDb->loadAssocList();
             
        }
        catch (RuntimeException $e)
        {
            JError::raiseWarning(500, $e->getMessage());
        }
        
        $resultArray = array();
        foreach($result as $tag) {
            array_push($resultArray, $tag['name']);
        }
        
        return $resultArray;
    }
    
    public function getEventSpeakers($id)
    {
        if($id <= 0) {
            //JError::raiseWarning(500, "Id must be a postive number");
            return array();
        }
        $result = array();
        
        $apiDb = DatabaseHelper::getApiDb();
        $query = $apiDb->getQuery(true)
        ->select('*')
        ->from($apiDb->quoteName('speakers', 's'))
        ->join('LEFT', $apiDb->quoteName('events_speakers', 'es') . ' ON es.speaker_id = s.id ')
        ->where($apiDb->quoteName('es.event_id') . ' = '.(int)$id);
        
        $apiDb->setQuery($query);
    
        try
        {
            $result = $apiDb->loadAssocList();
        }
        catch (RuntimeException $e)
        {
            JError::raiseWarning(500, $e->getMessage());
        }
        
        return $result;
    }
    
    public function getAllSpeakers()
    {
        $result = array();
        
        $apiDb = DatabaseHelper::getApiDb();
        $query = $apiDb->getQuery(true)
        ->select('*')
        ->from($apiDb->quoteName('speakers'));
        
        $apiDb->setQuery($query);
       
        try
        {
            $result = $apiDb->loadAssocList();
        }
        catch (RuntimeException $e)
        {
            JError::raiseWarning(500, $e->getMessage());
        }
        
        return $result;
    }
    
    public function getAllLocations()
    {
        $result = array();
        
        $apiDb = DatabaseHelper::getApiDb();
        $query = $apiDb->getQuery(true)
        ->select('*')
        ->from($apiDb->quoteName('locations'));
        
        $apiDb->setQuery($query);
       
        try
        {
            $result = $apiDb->loadAssocList();
        }
        catch (RuntimeException $e)
        {
            JError::raiseWarning(500, $e->getMessage());
        }
        
        return $result;
    }
    
    
    public function getEventLines($id)
    {
        if($id <= 0) {
            //JError::raiseWarning(500, "Id must be a postive number");
            return array();
        }
        $result = array();
        
        $apiDb = DatabaseHelper::getApiDb();
        $query = $apiDb->getQuery(true);
        $query->select('`l`.id');
        $query->from($apiDb->quoteName('research_lines', 'l'));
        $query->join('LEFT', $apiDb->quoteName('events_research_lines', 'ol') . ' ON l.id = ol.research_line_id');
        $query->where($apiDb->quoteName('event_id') . ' = ' . $id);
        
        $apiDb->setQuery($query);
        
        try
        {
            $result = $apiDb->loadAssocList();
        }
        catch (RuntimeException $e)
        {
            JError::raiseWarning(500, $e->getMessage());
        }
        
        return $result;
    }
    
    
    public function getCenterList()
    {
        $apiDb = DatabaseHelper::getApiDb();
        $query = $apiDb->getQuery(true);
        $query->select('*');
        $query->from($apiDb->quoteName('centers'));
        
        $apiDb->setQuery($query);
        
        try
        {
            $result = $apiDb->loadAssocList();
        }
        catch (RuntimeException $e)
        {
            JError::raiseWarning(500, $e->getMessage());
        }
        
        return $result;
    }
    
    
    private function saveEventLines($eventId, $lines, $insertOnly = false)
    {
        if((int)$eventId <= 0) {
            return false;
        }
        
        $db = DatabaseHelper::getApiDb();
        
        if(!$insertOnly) {
            $query = $db->getQuery(true);
            $query->delete($db->quoteName('events_research_lines'));
            $query->where( $db->quoteName('event_id') . ' = ' . (int)$eventId );
            $db->setQuery($query);
            $db->execute();
        }
        
        foreach($lines as $lineId) {
            $query = $db->getQuery(true);
            $query->insert( $db->quoteName('events_research_lines') );
            $query->columns( $db->quoteName(array('event_id', 'research_line_id')) );
            $query->values( implode(',', array($eventId, $lineId)) );
            $db->setQuery($query);
            $db->execute();
        }
        
        return true;
    }
    
    private function saveEventMedia($eventId, $media_url, $insertOnly = false)
    {
        if((int)$eventId <= 0) {
            return false;
        }
        
        $db = DatabaseHelper::getApiDb();

        if(!$insertOnly) {
            $query = $db->getQuery(true);
            $query->delete($db->quoteName('event_media'));
            $query->where( $db->quoteName('event_id') . ' = ' . (int)$eventId );
            $db->setQuery($query);
            $db->execute();
        }

        
        $query = $db->getQuery(true);
        $query->insert( $db->quoteName('event_media') );
        $query->columns( $db->quoteName(array('event_id', 'link')) );
        $query->values( implode(',', array($eventId,  $db->quote($media_url)) ) );
        $db->setQuery($query);
        $db->execute();
        

        return true;
    }
    
    private function saveEventSpeakers($eventId, $speakers, $insertOnly = false)
    {
        if((int)$eventId <= 0) {
            return false;
        }
        
        $db = DatabaseHelper::getApiDb();

        if(!$insertOnly) {
            $query = $db->getQuery(true);
            $query->delete($db->quoteName('events_speakers'));
            $query->where( $db->quoteName('event_id') . ' = ' . (int)$eventId );
            $db->setQuery($query);
            $db->execute();
        }

        foreach($speakers as $speakerId) {
            $query = $db->getQuery(true);
            $query->insert( $db->quoteName('events_speakers') );
            $query->columns( $db->quoteName(array('event_id', 'speaker_id')) );
            $query->values( implode(',', array($eventId, $speakerId)) );
            $db->setQuery($query);
            $db->execute();
        }

        return true;
    }
    
    /**
     * Method to save the events.
     * It sets error if validation does not pass.
     *
     * @param   array    $data      Data to save.
     * 
     * @return  boolean  true if saved, false otherwise.
     */
    public function  save($data)
    {/*
        $jinput = JFactory::getApplication()->input;
        $data = $jinput->get('jform',NULL,NULL);
        */
        $errors = array();
        $result = 0;
        
        
        if ( strlen($data['name_en']) == 0 || strlen($data['name_it']) == 0 ) {
            array_push($errors, 'Please fill out at least one name field');
        }
         
        if ( strlen($data['description_en']) == 0 || strlen($data['description_it']) == 0 ) {
            array_push($errors, 'Please fill out at least one description field');
        }
        
        if(count($errors) > 0) 
        {
            $this->setError('<br>' . implode('<br>', $errors));
            return false;
        }
        else 
        {
            if($data['id'] == 0) {
                $data['alias_it'] = JApplication::stringURLSafe($data['name_it']);
                $data['alias_en'] = JApplication::stringURLSafe($data['name_en']);
                
                $result = $this->insertEvent($data);
                //$result = $oModel->insertEvent($data);
            }
            else {
                if(!isset($data['alias_it']) || $data['alias_it'] == "")
                    $data['alias_it'] = JApplication::stringURLSafe($data['name_it']);
                
                if(!isset($data['alias_en']) || $data['alias_en'] == "")
                    $data['alias_en'] = JApplication::stringURLSafe($data['name_en']);
                
                $result = $this->updateEvent($data);
                //$result = $oModel->updateEvent($data);
            }
        }
    
        return true;
    }
    
    
    /**
     * Inserts new events to the database.
     * TO DO: add user data when we have it figured out in the api db.
     *
     * @param   array    $data      Data to insert.
     * 
     * @return  mixed  new event id if inserted, false otherwise.
     */
    public function insertEvent($data)
    {
        
        $db = DatabaseHelper::getApiDb();
        try
        {
            $db->transactionStart();
            
            $name_tid = TranslationHelper::saveTranslation('events','name', $data);
            $alias_tid = TranslationHelper::saveTranslation('events','alias', $data);
            $description_tid = TranslationHelper::saveTranslation('events','description', $data);
            
            if(!isset($data['videoconference']))
                $videoconference = 0;
            else
                $videoconference = 1;
            
            
            if(!isset($data['featured']))
                $featured = 0;
            else
                $featured = 1;
            
            $columns = array(
                'alias_tid',
                'name_tid',
                'description_tid',
                'start', 
                'end',
                'event_category_id',
                'center_id',
                'location_id',
                'room',
                'videoconference',
                'published',
                'created_on',
                'created_by',
                'modified_on',
                'modified_by',
                'access_type_id',
                'featured',
                'info_url',
                'contact_email',
                'contact_phone'
            );
            
            $values = array(
                (int) $alias_tid,
                (int) $name_tid,
                (int) $description_tid,
                $db->quote($data['start']),
                $db->quote($data['end']),
                (int) $data['event_category_id'],
                (int) $data['organized_by_center_id'],
                (int) $data['location_id'],
                $db->quote($data['room']),
                $db->quote($videoconference),
                (int) $data['published'],
                'NOW()',
                (int)1,//$db->quote($data['created_by']),
                'null',
                'null',
                (int) $data['access_type_id'],
                $db->quote($featured),
                $db->quote($data['info_url']),
                $db->quote($data['contact_email']),
                $db->quote($data['contact_phone'])
                
            );
            
            $query = $db->getQuery(true);
            $query->insert( $db->quoteName('events') );
            $query->columns( $db->quoteName($columns) );
            $query->values( implode(',', $values) );
            $db->setQuery($query);
            $db->execute();
            
            $resultId = $db->insertid();
            
            // Saving media
            if(isset($data['link']) && strlen($data['link']) > 0) 
            {
                $this->saveEventMedia($resultId, $data['link'], true);
            }
            
            // Saving speakers
            if(isset($data['speakers']) && strlen($data['speakers']) > 0) 
            {
                $speakers = explode(',', $data['speakers']);
                $this->saveEventSpeakers($resultId, $speakers, true);
            }
            
            // Saving lines
            if(count($data['lines']) > 0) 
            {
                $this->saveEventLines($resultId, $data['lines'], true);
            }
            
            // Saving tags
            if(isset($data['tags']) && strlen($data['tags']) > 0) 
            {
                $tagsArray = explode(',', $data['tags']);
                
                $tagsRes = EventTagsHelper::insertTags($tagsArray, $resultId);
            }
            
            $db->transactionCommit();
            return $resultId; 
        }
        catch (Exception $e)
        {
            $db->transactionRollback();
            JErrorPage::render($e);
            return false;
        }
        
    }
    
    /**
     * Updates Event in the database.
     * TO DO: add user data when we have it figured out in the api db.
     *
     * @param   array    $data      Data to update.
     * 
     * @return  boolean  true if updated, false otherwise.
     */
    public function updateEvent($data) 
    {
        $db = DatabaseHelper::getApiDb();
        
        
        try
        {
            $db->transactionStart();
           
            if(!isset($data['videoconference']))
                $videoconference = 0;
            else
                $videoconference = 1;
            
            
            if(!isset($data['featured']))
                $featured = 0;
            else
                $featured = 1;
            
            // save translations
            TranslationHelper::saveTranslation('events', 'alias', $data);
            TranslationHelper::saveTranslation('events', 'name', $data);
            TranslationHelper::saveTranslation('events', 'description', $data);
            
            $query = $db->getQuery(true);
            $fields = array(
                $db->quoteName('event_category_id') . ' = ' . (int)$data['event_category_id'],
                $db->quoteName('location_id') . ' = ' . (int)$data['location_id'],
                $db->quoteName('access_type_id') . ' = ' . (int)$data['access_type_id'],
                $db->quoteName('room') . ' = ' . (int)$data['room'],
                $db->quoteName('videoconference') . ' = ' . (int)$videoconference,
                $db->quoteName('start') . ' = ' . $db->quote($data['start']),
                $db->quoteName('end') . ' = ' . $db->quote($data['end']),
                $db->quoteName('published') . ' = ' . (int)$data['published'],
                $db->quoteName('featured') . ' = ' . (int)$featured,
                $db->quoteName('info_url') . ' = ' . $db->quote($data['info_url']),
                $db->quoteName('contact_email') . ' = ' . $db->quote($data['contact_email']),
                $db->quoteName('contact_phone') . ' = ' . $db->quote($data['contact_phone']),
                $db->quoteName('modified_on') . ' = NOW()',
                $db->quoteName('modified_by') . ' = '.(int)1//+ add user info
            );
            
            
            
            if($data['center_id'] != '0')
                {
                array_push($fields, $db->quoteName('center_id') . ' = ' . (int)$data['center_id']);
                }
            else{
                array_push($fields, $db->quoteName('center_id') . ' = NULL' );
                }
                
            
            $condition = $db->quoteName('id') . ' = ' . (int)$data['id'];
            $query->update($db->quoteName('events'));
            $query->set($fields);
            $query->where($condition);
            
            $db->setQuery($query);
            $db->execute();
            
            // Saving media
            if(isset($data['link']) && strlen($data['link']) > 0) 
            {
                $this->saveEventMedia($data['id'], $data['link']);
            }
            
            // Saving speakers
            if(isset($data['speakers']) && strlen($data['speakers']) > 0) 
            {
                $speakers = explode(',', $data['speakers']);
                $this->saveEventSpeakers($data['id'], $speakers);
            }
            
            // Saving tags
            if(isset($data['tags']) && strlen($data['tags']) > 0) 
            {
                $tagsArray = explode(',', $data['tags']);
                
                $tagsRes = EventTagsHelper::insertTags($tagsArray, $data['id'], false);
            }
            
            // Saving lines
            $this->saveEventLines($data['id'], $data['lines']);
            
            
            $db->transactionCommit();
            
            return $data['id']; 
        }    
        catch (Exception $e)
        {
            //die(var_dump($e));
            
            $db->transactionRollback();
            JErrorPage::render($e);
            return false;
        }
    }
    
    public function delete ($ids) 
    {
        if (count($ids) <= 0)
        {
            return array('status' => 'fail', 'error' => 'No ids selected.');
        }
        
        $db = DatabaseHelper::getApiDb();
        
         try
        {
            $db->transactionStart();
            // CANCELLARE TAGS REFERENCES
            
            $tagsDel = $this->deleteTagsReferences($ids);
            if(is_array($tagsDel))
            {
                return $tagsDel;
            }
            
            $query = $db->getQuery(true);
            $query->delete($db->quoteName('events'));
            $query->where($db->quoteName('id') . ' IN (' . implode(',', $ids). ')');
            $db->setQuery($query);
            $db->execute();
            
            $db->transactionCommit();
            return array('status' => 'ok', 'data'=> count($ids) . ' events deleted.');
        }
        catch (Exception $e)
        {
            $db->transactionRollback();
            return array('status' => 'fail', 'data'=> 'Database error.');
        }  
        
    }
    
    
    protected function deleteTagsReferences($ids) 
    {
        if (count($ids) <= 0)
        {
            return array('status' => 'fail', 'error' => 'No ids selected.');
        }
        
        $db = DatabaseHelper::getApiDb();
        
         try
        {
            $query = $db->getQuery(true);
            $query->delete($db->quoteName('events_tags'));
            $query->where($db->quoteName('event_id') . ' IN (' . implode(',', $ids). ')');
            $db->setQuery($query);
            $db->execute();
            
            return true;
        }
        catch (Exception $e)
        {
            return array('status' => 'fail', 'data'=> 'Database error.');
        }  
        
        
        
    }
    
}