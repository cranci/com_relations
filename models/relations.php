<?php
/**
 * @package  com_events
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_COMPONENT.'/helpers/database.php');

/**
 * EventsList Model
 *
 * @since  0.0.1
 */
class RelationsModelRelations extends JModelList
{
    
    public function __construct($config = array())
    {
	
        parent::setDbo(DatabaseHelper::getApiDb());
    }
    
    /**
     * Method to build an SQL query to load the list data.
     *
     * @return      string  An SQL query
     */
    
    public function getForm()
    {
        try {
            $form = JForm::getInstance('relations',JPATH_COMPONENT.'/models/forms/relations.xml');
            return $form;
        }
        catch (InvalidArgumentException $invalid){
            return false;
        }
        catch (RuntimeException $runtime){
            return false;
        }
        return new JForm();
    }
    
    protected function getListQuery()
    {
        $apiDb = $this->getDbo();
        $query = $apiDb->getQuery(true);
        $query->select('*');
        $query->from($apiDb->quoteName('view_events'));
        $apiDb->setQuery($query);
       
        try
        {
             $result = $apiDb->getQuery();
             //$result = $apiDb->loadObjectList();
        }
        catch (RuntimeException $e)
        {
            JError::raiseWarning(500, $e->getMessage());
        }
        /*
        print("<pre>");
        print_r($result);
        print("</pre>");
        die();*/
        return $result;
        
       
    }
    
    
    
    
}



        