<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_COMPONENT.'/helpers/database.php');
require_once(JPATH_COMPONENT.'/helpers/translation.php');
require_once(JPATH_COMPONENT.'/helpers/programslist.php');

class RelationsModelProgram extends JModelLegacy
{
    public function __construct() {
        parent::__construct();
        parent::setDbo(DatabaseHelper::getApiDb());
    }
    
    public function save($data)
    {
        $db = DatabaseHelper::getApiDb();
        try
        {
            $db->transactionStart();
            
            $name_tid = TranslationHelper::saveTranslation('programs','name', $data);
            $alias_tid = TranslationHelper::saveTranslation('programs','alias', $data);
            $description_tid = TranslationHelper::saveTranslation('programs','description', $data);
            
            $query = $db->getQuery(true);
            
            /////////////////////////////////////////
             if(is_numeric($data['id']) ) // UPDATE
                {
                $fields = array(
                    $db->quoteName('alias_tid') . ' = ' . (int) $alias_tid,
                    $db->quoteName('name_tid') . ' = ' . (int) $name_tid,
                    $db->quoteName('description_tid') . ' = ' . (int) $description_tid,
                    $db->quoteName('img_source') . ' = ' .  $db->quote($data['img_source']),
                    $db->quoteName('state') . ' = ' .  $data['state']
                ); 
                
                $condition = $db->quoteName('id') . ' = ' . (int)$data['id'];
                $query->update($db->quoteName('programs'));
                $query->set($fields);
                $query->where($condition);
                $db->setQuery($query);
                $db->execute();
                }
            else // INSERT
                {
                $columns = array(
                'alias_tid',
                'name_tid',
                'description_tid',
                'img_source',
                'state'
                );

                $values = array(
                    (int) $alias_tid,
                    (int) $name_tid,
                    (int) $description_tid,
                    $db->quote($data['img_source']),
                    (int) $data['state']
                );
                
                $query->insert( $db->quoteName('programs') );
                $query->columns( $db->quoteName($columns) );
                $query->values( implode(',', $values) );
                
                $db->setQuery($query);
                $db->execute();
                $data['id'] = $db->insertid();
                }
            /////////////////////////////////////////
            
            $db->transactionCommit();
           
            $response = array('done' => true, 'data'=>$data);
            return $response;
        }
        catch (Exception $e)
        {
            $db->transactionRollback();
            JErrorPage::render($e);
            return false;
        }
        
    }
    
    public function getProgramById($id){
        
        $line = ProgramslistHelper::getProgramById($id);
        $response = array('done' => true, 'data'=>$line);
        return $response;
    }
    
}

