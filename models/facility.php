<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_COMPONENT.'/helpers/database.php');
require_once(JPATH_COMPONENT.'/helpers/translation.php');
require_once(JPATH_COMPONENT.'/helpers/lineslist.php');
require_once(JPATH_COMPONENT.'/helpers/facilitieslist.php');

class RelationsModelFacility extends JModelLegacy
{
    public function __construct() {
        parent::__construct();
        parent::setDbo(DatabaseHelper::getApiDb());
    }
    /*
    public function save($data)
    {
        if($data['id'] <= 0) {
            $resData = $this->add($data);     
        }
        else{
            $resData = $this->add($data);   
        }
        
        $response = array('done' => true, 'data'=>$resData);
            return $response;
    }*/
    
    public function save($data)
    {
        $db = DatabaseHelper::getApiDb();
        try
        {
            $db->transactionStart();
            
            $name_tid = TranslationHelper::saveTranslation('facilities','name', $data);
            $alias_tid = TranslationHelper::saveTranslation('facilities','alias', $data);
            $description_tid = TranslationHelper::saveTranslation('facilities','description', $data);
           
            $query = $db->getQuery(true);
            
            if(is_numeric($data['id']) ) // UPDATE
                {
                $fields = array(
                    $db->quoteName('alias_tid') . ' = ' . (int) $alias_tid,
                    $db->quoteName('name_tid') . ' = ' . (int) $name_tid,
                    $db->quoteName('description_tid') . ' = ' . (int) $description_tid,
                    $db->quoteName('img_source') . ' = ' .  $db->quote($data['img_source']),
                    $db->quoteName('state') . ' = ' .  (int)$data['state']
                ); 
                
                $condition = $db->quoteName('id') . ' = ' . (int)$data['id'];
                $query->update($db->quoteName('facilities'));
                $query->set($fields);
                $query->where($condition);
                $db->setQuery($query);
                $db->execute();
                }
            else // INSERT
                {
                $columns = array(
                'alias_tid',
                'name_tid',
                'description_tid',
                'img_source',
                'state'
                );

                $values = array(
                    (int) $alias_tid,
                    (int) $name_tid,
                    (int) $description_tid,
                    $db->quote($data['img_source']),
                    (int) $data['state']
                );
                
                $query->insert( $db->quoteName('facilities') );
                $query->columns( $db->quoteName($columns) );
                $query->values( implode(',', $values) );
                
                $db->setQuery($query);
                $db->execute();
                $data['id'] = $db->insertid();
                }
            
            $db->transactionCommit();
           
            $response = array('done' => true, 'data'=>$data);
            return $response;
        }
        catch (Exception $e)
        {
            $db->transactionRollback();
            JErrorPage::render($e);
            /*
            $response = array('done' => true, 'data'=>$query);
            return $response;*/
            
            return false;
        }
        
    }
    
    
    public function saveRelations($data, $insertOnly = false)
    {
        $prg_ln = json_decode($data['programs_lines']);
        $ctr_ln = json_decode($data['centers_lines']);
        
        $this->saveProgramsLinesRelations($prg_ln);
        $this->saveCentersLinesRelations($ctr_ln);
        
        $response = array('done' => true, 'data'=>$data);
        return $response;
    }
    
    
    private function saveProgramsLinesRelations($prg_ln, $insertOnly=false)
    {
        $db = DatabaseHelper::getApiDb();

        foreach ($prg_ln as $lineId => $programsIds) 
            {
            if(!$insertOnly) {
                // PROGRAMS
                $queryOne = $db->getQuery(true);
                $queryOne->delete($db->quoteName('programs_research_lines'));
                $queryOne->where( $db->quoteName('research_line_id') . ' = ' . (int)$lineId );
                $db->setQuery($queryOne);
                $db->execute();
            }

            // LINE - PROGRAMS            
            foreach($programsIds as $programId) {
                $query = $db->getQuery(true);
                $query->insert( $db->quoteName('programs_research_lines') );
                $query->columns( $db->quoteName(array('program_id', 'research_line_id')) );
                $query->values( implode(',', array($programId, $lineId)) );
                $db->setQuery($query);
                $db->execute();
            }
            
        
        }
    }
    
    private function saveCentersLinesRelations($ctr_ln, $insertOnly=false)
    {
        $db = DatabaseHelper::getApiDb();

        foreach ($ctr_ln as $lineId => $centersIds) 
            {
            if(!$insertOnly) {
                // CENTERS
                $queryTwo = $db->getQuery(true);
                $queryTwo->delete($db->quoteName('centers_research_lines'));
                $queryTwo->where( $db->quoteName('research_line_id') . ' = ' . (int)$lineId );
                $db->setQuery($queryTwo);
                $db->execute();
            }

            // LINE - CENTERS
            foreach($centersIds as $centerId) {
                $query = $db->getQuery(true);
                $query->insert( $db->quoteName('centers_research_lines') );
                $query->columns( $db->quoteName(array('center_id', 'research_line_id')) );
                $query->values( implode(',', array($centerId, $lineId)) );
                $db->setQuery($query);
                $db->execute();
            }
        }
    }
    
    public function getFacilityById($id){
        $fac = FacilitieslistHelper::getFacilityById($id);
        $response = array('done' => true, 'data'=>$fac);
        return $response;
    }
    
}

