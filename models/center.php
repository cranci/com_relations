<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_COMPONENT.'/helpers/database.php');
require_once(JPATH_COMPONENT.'/helpers/translation.php');
require_once(JPATH_COMPONENT.'/helpers/centerslist.php');

class RelationsModelCenter extends JModelLegacy
{
    public function __construct() {
        parent::__construct();
        parent::setDbo(DatabaseHelper::getApiDb());
    }
    
    public function save($data)
    {
        
        
        $db = DatabaseHelper::getApiDb();
        try
        {
            $db->transactionStart();
            
          $query = $db->getQuery(true);
            
            /////////////////////////////////////////
             if(is_numeric($data['id']) ) // UPDATE
                {
                $fields = array(
                    $db->quoteName('name') . ' = ' .  $db->quote($data['name']),
                    $db->quoteName('shortcut') . ' = ' .  $db->quote($data['shortcut']),
                    $db->quoteName('alias') . ' = ' .  $db->quote($data['alias']),
                    $db->quoteName('country') . ' = ' .  $db->quote($data['country']),
                    $db->quoteName('city') . ' = ' .  $db->quote($data['city']),
                    $db->quoteName('address') . ' = ' .  $db->quote($data['address']),
                    $db->quoteName('zip') . ' = ' .  $db->quote($data['zip']),
                    $db->quoteName('phone') . ' = ' .  $db->quote($data['phone']),
                    $db->quoteName('lat') . ' = ' .  (float)$data['lat'],
                    $db->quoteName('lng') . ' = ' .  (float)$data['lng'],
                    $db->quoteName('state') . ' = ' .  (int)$data['state']
                ); 
                
                $condition = $db->quoteName('id') . ' = ' . (int)$data['id'];
                $query->update($db->quoteName('centers'));
                $query->set($fields);
                $query->where($condition);
              //$response = array('done' => true, 'data'=>$query->__toString());
           // return $response;
                $db->setQuery($query);
                $db->execute();
                }
            else // INSERT
                {
                 $columns = array(
                'name',
                'shortcut',
                'alias',
                'country',
                'city',
                'address',
                'zip',
                'phone',
                'lat',
                'lng',
                'state'
            );
            
            $values = array(
                $data['name'],
                $data['shortcut'],
                $data['alias'],
                $data['country'],
                $data['city'],
                $data['address'],
                $data['zip'],
                $data['phone'],
                $data['lat'],
                $data['lng'],
                $data['state']
            );
            
            $query->insert( $db->quoteName('centers') );
            $query->columns( $db->quoteName($columns) );
            $query->values( implode(',', $values) );
            
            $db->setQuery($query);
            $db->execute();
            
            $data['id'] = $db->insertid();
                }
            /////////////////////////////////////////
            $db->transactionCommit();
           
            $response = array('done' => true, 'data'=>$data);
            return $response;
        }
        catch (Exception $e)
        {
            $db->transactionRollback();
            JErrorPage::render($e);
            return false;
        }
        
    }
    
    public function getCenterById($id){
        
        $line = CenterslistHelper::getCenterById($id);
        $response = array('done' => true, 'data'=>$line);
        return $response;
    }
    
}

